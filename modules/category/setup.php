<?php
/**
 * Module: Category
 * Desription: Category management
 * in product creation
 * 
 * @since  1.2
 */
class NYPIZZA_CATEGORY
{
		
		public $slug = 'nypizza_category';
		public $folder = 'category';		 
		public $table = 'pizza_category';



		/**
		 * Default function to load
		 * after instantiation
		 *
		 * @since  1.2
		 */
		function __construct()
		{

			$this->media();
			$this->dependencies();
			$this->hooks();
			
		}



		/**
		 * Hooks
		 * wp add hooks
		 *
		 * @since  1.2
		 */
		public function hooks()
		{

			add_action( 'admin_menu', array( $this, 'menu' ) );

			add_action('wp_ajax_category_orders', 'pizza_category_order_update');
			add_action('wp_ajax_nopriv_category_orders', 'pizza_category_order_update');

		}



		/**
		 * Options Dependencies
		 * load dependencies
		 *
		 * @since  1.2
		 */
		public function dependencies()
		{			
			require_once( NYPIZZA_MODULES . "/{$this->folder}/admin/category.php" );
		}



		/**
		 * Options Admin Menu
		 * add menu in dashboard
		 *
		 * @since  1.2
		 */
		public function menu()
		{
			add_menu_page(
		        'Category Management',
		        'Category Management',
		        'manage_options',
		        $this->slug,
		        'pizza_category_management',
		        NYPIZZA_URI . '/media/icons/icon.png',
		        30
		    );
		}



		/**
		 * Options Media
		 * styles and scripts
		 *
		 * @since  1.2
		 */
		public function media()
		{

			wp_register_style(
				'pizza-category-css',
				NYPIZZA_URI . '/media/css/category.css'
			);		
			
			wp_register_style(
				'sortable-css',
				'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
			);

			wp_register_script(
				'sortable-script',
				'//code.jquery.com/ui/1.12.1/jquery-ui.js'
			);

			wp_register_script(
				'pizza-category-script',
				NYPIZZA_URI . '/media/script/category.js',
				array('jquery')
			);
			wp_localize_script(
				'pizza-category-script',
				'ajax_object',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) 
			);

		}


}