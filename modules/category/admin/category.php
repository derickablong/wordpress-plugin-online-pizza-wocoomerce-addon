<?php
/**
 * Module: Category
 */
function pizza_category_management() {
	global $nypizza;

	wp_enqueue_style('pizza-category-css');
	wp_enqueue_style('sortable-css');
	wp_enqueue_script('sortable-script');
	wp_enqueue_script('pizza-category-script');

	pizza_category_add();
	pizza_category_update();
	?>


	<div class="category-wrap wrap">
		<h2>Category Management</h2>
		<p>To udpate category, click the category name. <br>To reorder the categories, drag and drop the category.</p>

		<?php $nypizza->message(); ?>

		<div class="category-form category-form-add" style="display:block">
			<form action="" method="post">				
				<label for="category-name">Name</label>
				<input type="text" name="category-name" id="category-name" value="">
				<input type="submit" value="Add new category" name="category-add" class="button button-primary">
			</form>
		</div>

		<div class="category-form category-form-edit">
			<form action="" method="post">
				<a href="#" class="close-update">x</a>
				<input type="hidden" name="category-id" id="category-id" value="0">
				<label for="category-name">Name</label>
				<input type="text" name="category-name" id="category-name" value="">
				<input type="submit" value="Update Category" name="category-update" class="button button-primary">
			</form>
		</div>

		<div class="created-categories">			
			<?php pizza_category_all(); ?>			
		</div>
		
	</div>


	<?php
}




function pizza_category_all() {

	
	$taxonomy     = 'product_cat';
	$orderby      = 'meta_value_num';  
	$show_count   = 0;      
	$pad_counts   = 0;      	
	$title        = '';  
	$empty        = 0;

	$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'order'        => 'ASC',
			'title_li'     => $title,
			'hide_empty'   => $empty,
			'meta_query' => [[
				'key' => 'order',
				'type' => 'NUMERIC',
			]],			
	);

	$all_categories = get_terms( $args );

	foreach ($all_categories as $cat) {

		if($cat->category_parent == 0 && strtolower( $cat->name ) != 'uncategorized') { ?>
		
		<a href="#" data-category="<?php echo $cat->term_id ?>" class="category-order" title="Click to edit">
			<span class="cat-drag"></span>
			<span class="cat-name"><?php echo $cat->name ?></span>
		</a>		

		<?php
		}
	}
	

}




function pizza_category_update() {
	global $nypizza;

	if (isset($_POST['category-update'])) {		

		$cat_id = $_POST['category-id'];
		$name = $_POST['category-name'];
		$slug = str_replace(' ', '-', strtolower( $name ));

		if (!empty( $name )) {
			wp_update_term($cat_id, 'product_cat', array(
				'name' => $name,
				'slug' => $slug
			));			
			$nypizza->message['success'][] = 'Category updated.';
		} else {
			$nypizza->message['error'][] = 'Category name was empty.';
		}
		
	}
}




function pizza_category_add() {
	global $nypizza;

	if (isset($_POST['category-add'])) {		
	
		$name = $_POST['category-name'];
		$slug = str_replace(' ', '-', strtolower( $name ));

		if (!empty( $name )) {
			$cid = wp_insert_term(
				$name, 
				'product_cat', 
				array(					
					'slug' => $slug,				
				)
			);		
			$nypizza->message['success'][] = 'Category added.';
		} else {
			$nypizza->message['error'][] = 'Category name was empty.';
		}
		
	}
}





function pizza_category_order_update() {
	global $wpdb;

	$table = $wpdb->prefix . 'termmeta';
	$orders = $_POST['orders'];

	foreach ($orders as $order) {		

		$order = (object) $order;

		update_term_meta(
			$order->term_id,
			'order',
			$order->order
		);		

	}

	wp_send_json(
		array(
			'orders' => $orders,
			'query' => $query
		)
	);
	wp_die();
}