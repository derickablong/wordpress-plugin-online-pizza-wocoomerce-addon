var data_products = [[{
ID: 757,
name: '12 Inch Cheese Pizza',
description: '',
price: 14.99,
quantity: 1,
total: 14.99,
status: 'processing',
options: [
[{
ID: 2,
name: 'Meats',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 3,
name: 'Veggies',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 4,
name: 'Extras',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 312,
name: '18 Inch Cheese Pizza',
description: '',
price: 24.99,
quantity: 1,
total: 24.99,
status: 'processing',
options: [
[{
ID: 2,
name: 'Meats',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 2.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 3,
name: 'Veggies',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 4,
name: 'Extras',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 238,
name: '2 Liter Soda',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 58,
name: 'Drinks',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepsi',
description: '',
amount: 2,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mountain Dew',
description: '',
amount: 2,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Diet Pepsi',
description: '',
amount: 2,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Root Beer',
description: '',
amount: 2,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sierra Mist',
description: '',
amount: 2,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 180,
name: '2 Slices of Regular Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 1,
name: 'Cheese',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Buffalo Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Feta Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 2,
name: 'Meats',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 3,
name: 'Veggies',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 4,
name: 'Extras',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 5,
name: 'Bread Sizes',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [

]
}]
,[{
ID: 6,
name: 'Garlic Knots Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 piece',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 7,
name: 'Bosco Sticks Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 248,
name: '2 Slices of Specialty Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [

]
}],[{
ID: 260,
name: '343 Buffalo Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 52,
name: '343 Buffalo Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Chicken',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mild Buffalo Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 411,
name: 'Bacon Cheddar Broccoli Bites',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 45,
name: 'Bacon Cheddar Broccoli Bites',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 71,
name: 'Non-Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 259,
name: 'BBQ Jail Bird Chicken Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 10,
name: 'BBQ Jail Bird Chicken Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Chicken',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Basil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 732,
name: 'Big Billy’s BLT Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 19,
name: 'Big Billy’s BLT Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Apple-wood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Grape Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade Buttermilk Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 266,
name: 'Bone-In Chicken Wings',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 37,
name: 'Bone-In Chicken Wings',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 70,
name: 'Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Hot Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mild Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ with a kick',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Imported Teriyaki',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic Parmesan',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 145,
name: 'Boneless Chicken Wings',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 38,
name: 'Boneless Chicken Wings',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 70,
name: 'Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Hot Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mild Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ with a kick',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Imported Teriyaki',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic Parmesan',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 328,
name: 'Bosco Sticks',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 7,
name: 'Bosco Sticks Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 158,
name: 'Bottle of Water 16 oz',
description: '',
price: 1.00,
quantity: 1,
total: 1.00,
status: 'processing',
options: [

]
}],[{
ID: 641,
name: 'C.B.R. Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 33,
name: 'C.B.R. Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Applewood Organic Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Locally Sourced Chicken',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 247,
name: 'Calzone',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 73,
name: 'Meats',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 74,
name: 'Veggies',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 76,
name: 'Extras',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 243,
name: 'Cheese Curds',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 41,
name: 'Cheese Curds',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Small',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Medium',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Large',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 71,
name: 'Non-Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 623,
name: 'Chef Kris’ California Kobb Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 17,
name: 'Chef Kris’ California Kobb Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'White Sauce Base',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Chicken',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Apple-wood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Avocados Chunks',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Barbecue Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'House-made Buttermilk Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 618,
name: 'Chicken Parmesan Sandwich',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 63,
name: 'Chicken Parmesan Sandwich',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '1 piece',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 72,
name: 'Chips',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Sour Cream',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 619,
name: 'Eggplant Parmesan Sandwich',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 64,
name: 'Eggplant Parmesan Sandwich',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '1 piece',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 72,
name: 'Chips',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Sour Cream',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 255,
name: 'Garden Delight Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 14,
name: 'Garden Delight Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mixed Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Brocolli',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Spinach',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mushrooms',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 245,
name: 'Garlic Knots',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 6,
name: 'Garlic Knots Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 piece',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 637,
name: 'Hell&#8217;s Kitchen Chicken Parmesan Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 28,
name: 'Hell’s Kitchen Chicken Parmesan Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Amish Farm Raised Chicken',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Buffalo Mozzarella',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Basil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 257,
name: 'M.O.M. Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 12,
name: 'M.O.M. Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fontanini Meatballs',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mushrooms',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 0,
name: '',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [

]
}]

]
}],[{
ID: 244,
name: 'Mac and Cheese Bites',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 40,
name: 'Mac and Cheese Bites',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 71,
name: 'Non-Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 262,
name: 'Magnificent Meatball Sandwich',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 61,
name: 'Magnificent Meatball Sandwich',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '1 piece',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 72,
name: 'Chips',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Sour Cream',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 597,
name: 'Mama Luciano&#8217;s Lasagna Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 30,
name: 'Mama Luciano’s Lasagna Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Fontanini Meatballs',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Homemade Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mozzarella',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Ricotta Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Garlic',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Meatballs',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 258,
name: 'Margarita Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 11,
name: 'Margarita Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Basil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 261,
name: 'Marvelous Beef Sandwich',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 62,
name: 'Marvelous Beef Sandwich',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '1 piece',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 72,
name: 'Chips',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Sour Cream',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 638,
name: 'P.O.P. Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 29,
name: 'P.O.P. Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Pepperoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mixed Sweet Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 642,
name: 'Pineapple Express Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 34,
name: 'Pineapple Express Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Pineapple',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Honey Ham',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 204,
name: 'Poblano Poppers',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 47,
name: 'Poblano Poppers',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 71,
name: 'Non-Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 264,
name: 'Pork Wings',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 39,
name: 'Pork Wings',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 70,
name: 'Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Hot Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mild Buffalo',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ with a kick',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Imported Teriyaki',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic Parmesan',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 639,
name: 'Porky&#8217;s Paradise Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 31,
name: 'Porky’s Paradise Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Honey Ham',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Applewood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Pineapple Chunks',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade Pulled Pork',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade BBQ Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Cilantro',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 640,
name: 'Porky&#8217;s Revenge Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 32,
name: 'Porky’s Revenge Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Honey Ham',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Applewood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Pineapple Chunks',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade Pulled Pork',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Homemade BBQ Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Cilantro',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Serrano Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Banana Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: '“Green Monster” Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 201,
name: 'Regular Slice',
description: '',
price: 4,
quantity: 1,
total: 4,
status: 'processing',
options: [

]
}],[{
ID: 250,
name: 'Regular Slice of Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 1,
name: 'Cheese',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Buffalo Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Feta Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 2,
name: 'Meats',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 3,
name: 'Veggies',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 4,
name: 'Extras',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 5,
name: 'Bread Sizes',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [

]
}]
,[{
ID: 6,
name: 'Garlic Knots Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 piece',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 7,
name: 'Bosco Sticks Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 718,
name: 'Sauces and Dressings',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 65,
name: 'Sauces & Dressings',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Barbecue sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mild sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Hot sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Teriyaki sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic parm sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Strawberry vinaigrette',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Aged balsamic vinegar',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Extra virgin olive oil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Extra virgin olive oil and balsamic vinegar salad dressing',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Green Goddess Dressing',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Cranberry Vinaigrette',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 600,
name: 'Snow White Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 16,
name: 'Snow White Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Fresh Organic Ricotta Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fresh Organic Spinach',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fresh Organic Zima',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Grape Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fresh Organic Garlic',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 249,
name: 'Specialty Pizza Slice',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 1,
name: 'Cheese',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Buffalo Mozzarella Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Feta Cheese',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 2,
name: 'Meats',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 3,
name: 'Veggies',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 4,
name: 'Extras',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 1,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 5,
name: 'Bread Sizes',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [

]
}]
,[{
ID: 6,
name: 'Garlic Knots Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 piece',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 7,
name: 'Bosco Sticks Sizes',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '6 pieces',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '12 pieces',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '24 pieces',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 203,
name: 'Specialty Slice',
description: '',
price: 5,
quantity: 1,
total: 5,
status: 'processing',
options: [

]
}],[{
ID: 263,
name: 'Spiedies Sandwich',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 60,
name: 'Spiedies Sandwich',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '1 piece',
description: '',
amount: 7.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 72,
name: 'Chips',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Sour Cream',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'BBQ',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 246,
name: 'Stromboli',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 77,
name: 'Extras',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 1,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 78,
name: 'Meats',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Pepperoni',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sausage',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Bacon',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Meatballs',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Beef',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Ham',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Chicken',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Shrimp',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Prosciutto',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 79,
name: 'Veggies',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Mixed Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Jalapeno Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Mushrooms',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Tomatoes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Garlic',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Basil',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Onions',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Pineapple',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Black Olives',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Olive Medley',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Broccoli',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Spinach',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Roasted Red Peppers',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Asparagus',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Artichokes',
description: '',
amount: 1.5,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 314,
name: 'Sweet Potato Fries',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 43,
name: 'Sweet Potato Fries',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Small',
description: '',
amount: 9.99,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Medium',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Large',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 71,
name: 'Non-Wings Sauce',
selected: 0,
multiple: 0,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: 'Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Blue Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 251,
name: 'Swimming with the Shrimps Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 13,
name: 'Swimming with the Shrimps Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Spinach',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Garlic',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mixed Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Shrimp',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Virgin Olive Oil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 635,
name: 'The Bensonhurst Bruiser',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 26,
name: 'The Bensonhurst Bruiser',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Sausage',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Pepperoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Sliced Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Jalapenos',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Garlic',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 624,
name: 'The Big C Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 18,
name: 'The Big C Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Chicken Breast',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fresh Romaine Lettuce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'House-made Buttermilk Ranch',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Parmesan Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 626,
name: 'The Chelsea Street Delight Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 20,
name: 'The Chelsea Street Delight Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Grape Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Italian Prosciutto',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 252,
name: 'The Chicago Way Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 8,
name: 'The Chicago Way Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Fontanini Italian Beef',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Mixed Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Home-Made Giardiniera',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 629,
name: 'The Five Points of New York Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 22,
name: 'The Five Points of New York Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Artichoke',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Roasted Red Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Sun Dried Tomatoes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Black Olives',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Prosciutto',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 634,
name: 'The Four Hooved Pig Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 25,
name: 'The Four Hooved Pig Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Fontanini Sausage',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Pepperoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Black Forest Honey Ham',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Apple-wood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 253,
name: 'The Greasy Man&#8217;s Special Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 9,
name: 'The Greasy Man’s Special Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Mozzarella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fontanini Meatballs',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fontanini Sausage',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fontanini Pepporoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Fontanini Ham',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 636,
name: 'The New York Minute Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 27,
name: 'The New York Minute Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Pepperoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Sliced Banana Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 633,
name: 'The Parthenon Plaza Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 24,
name: 'The Parthenon Plaza Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 254,
name: 'The White Delight Pizza',
description: '',
price: 0.00,
quantity: 1,
total: 0.00,
status: 'processing',
options: [
[{
ID: 15,
name: 'White Delight Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Mozerella Cheese',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Spinach',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Herb Artichokes',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Brocolli',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 628,
name: 'Tommy&#8217;s “TREE” Meat and Onions Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 21,
name: 'Tommy’s “TREE” Meat and Onions Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Marinara Red Sauce',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Pepperoni',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Fontanini Sausage',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Apple-wood Smoked Bacon',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Peppers',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Red Onions',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}],[{
ID: 630,
name: 'Uncle Eddie&#8217;s Eggplant Pizza',
description: '',
price: 0,
quantity: 1,
total: 0,
status: 'processing',
options: [
[{
ID: 23,
name: 'Uncle Eddie’s Eggplant Pizza',
selected: 0,
multiple: 1,
is_required: 0,
allow_half: 0,
allow_quantity: 0,
is_logic: 0,
logic: '',
preselected: 1,
options: [
[{
name: 'Organic Eggplant',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Marinara',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Buffalo Mozzarella',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]
,[{
name: 'Organic Basil',
description: '',
amount: 0,
selected: 0,
type: 'Whole'
}]

]
}]
,[{
ID: 69,
name: 'Pizza Size',
selected: 0,
multiple: 1,
is_required: 1,
allow_half: 0,
allow_quantity: 1,
is_logic: 0,
logic: '',
preselected: 0,
options: [
[{
name: '12 Inch',
description: '',
amount: 19.99,
selected: 0,
type: 'Whole'
}]
,[{
name: '18 Inch',
description: '',
amount: 29.99,
selected: 0,
type: 'Whole'
}]

]
}]

]
}]];