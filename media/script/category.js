var cat = jQuery.noConflict();

NYPIZZA_CATEGORY = {

    build: function() {
        NYPIZZA_CATEGORY.sortCategory();
        NYPIZZA_CATEGORY.actions();
    },


    editCategory: function(e) {
        cat('.category-form').hide();

        var $cat = cat(e.target).closest('.category-order');
        var $form = cat('.category-form-edit');

        var id = $cat.data('category');
        var name = $cat.find('.cat-name').text();        

        $form.show();

        $form.find('#category-id').val(id);
        $form.find('#category-name')
            .val(name)
            .focus()
            .select();
        
    },


    control: function(e, callback) {
		e.preventDefault();
		e.stopPropagation();
		callback(e);
	},


    sortCategory: function() {
        cat( ".created-categories" )
            .sortable({
                stop: NYPIZZA_CATEGORY.udpateCatOrder
            })
            .disableSelection();
    },


    udpateCatOrder: function( event, ui ) {
        
        var orders = [];
        var $catergories = cat('.created-categories a');
        $catergories.each(function(index, el) {

            var $cat = cat(this);
            var cat_id = $cat.data('category');

            orders.push({
                term_id: cat_id,
                order: (index + 1)
            });

        });

        NYPIZZA_CATEGORY.server({
            action: 'category_orders',
            orders: orders
        }, function(response) {

            cat('.notice').remove();

            var message = `
            <div class='notice notice-success is-dismissible'>				
                <p>Category orders saved.</p>        
            </div>
            `;

            cat(message).insertAfter(cat('.category-wrap h2'));

        });

    },


    server: function( data, callback ) {
		console.log('Processing request...');
	
		cat.ajax({
			url: ajax_object.ajax_url,
			type: 'POST',			
			data: data,
			dataType: 'JSON',
			cache: false
		})
		.done(function(results) {							
			
			console.log('response received.');

			callback( results );
		});
	},


    actions: function() {

        var $doc = cat(document);

        $doc.on('click', '.created-categories a', function(e) {
            NYPIZZA_CATEGORY.control(e, NYPIZZA_CATEGORY.editCategory);
        });

        $doc.on('click', '.close-update', function(e) {
            NYPIZZA_CATEGORY.control(e, function(e) {
                cat('.category-form').hide();
                cat('.category-form-add').show();
            });
        });

    }

}
NYPIZZA_CATEGORY.build();