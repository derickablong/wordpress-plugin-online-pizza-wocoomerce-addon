jQuery(document).ready(function($) {
    
    /**
     * Change checkout button
     * text
     */
    var btn_interval;
    var active_percent = '10%';
    var update_tip = function() {
        btn_interval = setInterval(function() {            
            $('button#place_order').text('Proceed to Payment');	
            $('.tip-field, tr.fee').hide();

            $('select#tip-percent').val(active_percent);
    
            if ($('#tip-percent').val() === 'other') {
                $('.tip-percent-amount').hide();
                $('.tip-field').show();
                $('#order-tip').val($('#add_order_tip').val());
            } else {
                $('.tip-percent-amount')
                    .text( '$' + $('#add_order_tip').val() )
                    .show();
                $('.tip-field').hide();            
                $('#order-tip').val('');
            }
    
        }, 500);
    };		
    update_tip();

    $(window).on('load', function() {        
        $('body').trigger('update_checkout');
    });

    
    
    var typingTimer;                //timer identifier
    var doneTypingInterval = 5000;  //time in ms, 5 second for example   
    
    //disable enter
    $(document).on('keypress', '#order-tip').keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

    //on keyup, start the countdown
    $(document).on('keyup', '#order-tip', function (event) {
        clearInterval(btn_interval);
        $('#place_order')
            .prop('disabled', true)
            .text('Updating order...');
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $(document).on('keydown', '#order-tip', function () {
        clearInterval(btn_interval);
        $('#place_order')
            .prop('disabled', true)
            .text('Updating order...');
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
        $('#place_order')
            .prop('disabled', true)
            .text('Proceed to Payment');
        $('#add_order_tip').val( $('#order-tip').val() );
        $('body').trigger('update_checkout');
        update_tip();
    }

    /**
     * On change tip percent
     */
    $(document).on('change', '#tip-percent', function() {        

        var percent = $(this).val(); 
        active_percent = percent;
        
        var subtotal_text = ($('.cart-subtotal .amount').text()).replace('$', '');
        var subtotal = parseFloat( subtotal_text );

        switch (percent) {
			case '10%':
				$percent = 0.10; 				
				break;
			case '20%': 
				$percent = 0.20; 				
				break;
			case '30%': 
				$percent = 0.30; 				
                break;            
        }
        
        subtotal = subtotal * $percent;        
        
        $('#order-tip').val(subtotal.toFixed(2));
        $('input#add_order_tip').val(subtotal.toFixed(2));
        $('body').trigger('update_checkout');
    });

});